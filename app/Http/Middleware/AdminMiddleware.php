<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth::check() && (Auth::user()->role == "ADMIN" || Auth::user()->role == "DEKANAT" || Auth::user()->role == "BEM")){
            return $next($request);
        }
        return redirect()->route('login');
    }
}
