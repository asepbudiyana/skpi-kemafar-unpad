<?php

namespace App\Http\Controllers\Admin;

use App\Models\Application;
use App\Models\SupportData;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {

        return view('admin.home');
    }

    public function viewApplications()
    {
        $itemPerPage = 3;

        $applications = Application::join('users', 'users.id', '=', 'applications.user_id')
        ->select('applications.*', 'users.name')
        ->latest()
        ->paginate($itemPerPage);

        if (Auth::user()->role == "DEKANAT") {
            $applications_pending = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name')
            ->where('dekanat_confirmed', 0)
            ->latest()
            ->paginate($itemPerPage);

            $applications_accepted = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name')
            ->where('dekanat_confirmed', 1)
            ->latest()
            ->paginate($itemPerPage);

            $applications_declined = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name')
            ->where('dekanat_confirmed', 2)
            ->latest()
            ->paginate($itemPerPage);
        } else if (Auth::user()->role == "BEM") {
            $applications_pending = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name')
            ->where('bem_confirmed', 0)
            ->latest()
            ->paginate($itemPerPage);

            $applications_accepted = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name')
            ->where('bem_confirmed', 1)
            ->latest()
            ->paginate($itemPerPage);

            $applications_declined = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name')
            ->where('bem_confirmed', 2)
            ->latest()
            ->paginate($itemPerPage);
        } else {
            // Error
        }

        return view('admin.applications',compact('applications', 'applications_pending', 'applications_accepted', 'applications_declined'))
                ->with('i', (request()->input('page', 1) - 1) * $itemPerPage);
    }

    public function showApplications($id)
    {
        $application = Application::join('users', 'users.id', '=', 'applications.user_id')
            ->select('applications.*', 'users.name', 'users.npm')
            ->where('applications.id', $id)
            ->first();

        $support_datas = SupportData::where('application_id', $id)->get();
        foreach ($support_datas as $item) {
            $item->full_file = url('/application_file') . "/" . $item->file;
        }

        // $application->name = Auth::user()->name;
        return view('admin.applications_show',compact('application', 'support_datas'))->with('i');
    }

    public function acceptApplications(Request $request, Application $application)
    {
        if (Auth::user()->role == "DEKANAT") {
            $application->dekanat_confirmed = 1;
        } else if (Auth::user()->role == "BEM") {
            $application->bem_confirmed = 1;
        } else {
            // Error
        }
        
        $application->save();

        return redirect()->route('admin.applications.index')
                        ->with('success','SKPI Berhasil Dikonfirmasi');
    }

    public function declineApplications(Request $request, Application $application)
    {
        if (Auth::user()->role == "DEKANAT") {
            $application->dekanat_confirmed = 2;
        } else if (Auth::user()->role == "BEM") {
            $application->bem_confirmed = 2;
        } else {
            // Error
        }
        
        $application->save();

        return redirect()->route('admin.applications.index')
                        ->with('success','SKPI Berhasil Ditolak');
    }
}
