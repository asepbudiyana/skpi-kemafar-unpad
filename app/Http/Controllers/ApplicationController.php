<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\SupportData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = Application::join('users', 'users.id', '=', 'applications.user_id')
                        ->select('applications.*', 'users.name')
                        ->where('user_id', Auth::user()->id)
                        ->latest()
                        ->paginate(5);

        return view('applications.index',compact('applications'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $activities = [
            ['id' => "PDK01",'activity' => "Penghargaan dan Kejuaraan Internasional"],
            ['id' => "PDK02",'activity' => "Penghargaan dan Kejuaraan Nasional"],
            ['id' => "PDK03",'activity' => "Penghargaan dan Kejuaraan Kota/Provinsi"],
            ['id' => "PDK04",'activity' => "Penghargaan dan Kejuaraan Universitas"],
            ['id' => "PDK05",'activity' => "Penghargaan dan Kejuaraan Fakultas"],
            ['id' => "ORG01",'activity' => "Organisasi Internasional"],
            ['id' => "ORG02",'activity' => "Organisasi Nasional"],
            ['id' => "ORG03",'activity' => "Organisasi Daerah"],
            ['id' => "ORG04",'activity' => "Organisasi Universitas"],
            ['id' => "ORG05",'activity' => "Organisasi Fakultas"],
            ['id' => "PNT01",'activity' => "Kepanitiaan Internasional"],
            ['id' => "PNT02",'activity' => "Kepanitiaan Nasional"],
            ['id' => "PNT03",'activity' => "Kepanitiaan Daerah"],
            ['id' => "PNT04",'activity' => "Kepanitiaan Universitas"],
            ['id' => "PNT05",'activity' => "Kepanitiaan Fakultas"],

            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],
            // ['id' => ,'activity' => ],

            // "Penguasaan Bahasa Asing",
            // "Sertifikasi profesi dan kompetensi",

            // "Karir dan kewirausahaan",

            // "Upacara Kenegaraan",

            // "Menulis Buku ISBN",

            // "Kontribusi Kegiatan Sosial",

            // "Asisten Proyek/Riset/Laboratorium",

            // "Pertukaran Mahasiswa",

            // "Pembicara Kegiatan",

            // "Peserta Seminar",

            // "Delegasi",

            // "Publikasi Ilmiah Internasional",
            // "Publikasi Ilmiah Nasional Terakreditasi",
            // "Publikasi Ilmiah Nasional Belum Terakreditasi",
            // "Publikasi Ilmiah Unpad",

            // "Artikel Media Massa",

            // "Pelatihan Kepemimpinan",

            // "Mawapres"
        ];
        return view('applications.create', [
            'user' => $user,
            'activities' => $activities
        ]);
    }

    public function getClassification(Request $request)
    {
        $id = $request->get('id');
        if ($id == "PDK01") {
            $classifications = [
                ["id" => $id, "classification" => "3 Besar"],
                ["id" => 2, "classification" => "Harapan atau Favorit"],
                ["id" => 3, "classification" => "Peserta"]
            ];
        } else {
            $classifications = [
                ["id" => 0, "classification" => "Nothing"],
            ];
        }
        return response()->json($classifications);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'addMoreInputFields.*.activity' => 'required',
            'addMoreInputFields.*.classification' => 'required',
            'addMoreInputFields.*.file' => 'required|file|max:2048'
            // 'name' => 'required',
            // 'detail' => 'required',
        ]);
    
        // Application::create($request->all());

        $application = new Application;
        $application->user_id = Auth::user()->id;
        $application->bem_confirmed = 0;
        $application->dekanat_confirmed = 0;
        $application->save();

        foreach ($request->addMoreInputFields as $key => $value) {
            
            $file = $value['file'];
            $filename = time()."_".$file->getClientOriginalName();
            $destination = 'application_file';
            $file->move($destination,$filename);

            SupportData::create([
                'activity' => $value['activity'],
                'classification' => $value['classification'],
                'grade' => 0,
                'file' => $filename,
                'application_id' => $application->id
            ]);
        }
     
        return redirect()->route('user.applications.index')
                        ->with('success','Pengajuan SKPI Baru Berhasil Dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        $application->name = Auth::user()->name;
        return view('applications.show',compact('application'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        return view('applications.edit',compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        // $request->validate([
        //     'name' => 'required',
        //     'detail' => 'required',
        // ]);
    
        // $application->update($request->all());
    
        return redirect()->route('user.applications.index')
                        ->with('success','Pengajuan SKPI Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        $application->delete();
    
        return redirect()->route('user.applications.index')
                        ->with('success','Pengajuan Berhasil Dibatalkan');
    }

    public function printPdf(Application $application)
    {
        // $applications = Application::join('users', 'users.id', '=', 'applications.user_id')
        // ->select('applications.*', 'users.name')
        // ->get();

        $application->name = Auth::user()->name;
        $application->npm = Auth::user()->npm;
        $support_datas = SupportData::where('application_id', $application->id)->get();
    	$pdf = PDF::loadview('applications.skpi_pdf',[
            'application' => $application, 
            'support_datas' => $support_datas]);
    	// return $pdf->download('print-skpi-pdf');
        return $pdf->stream('print-skpi-pdf');
    }
}
