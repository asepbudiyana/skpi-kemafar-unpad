<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);
 
Route::group(['middleware' => 'auth'], function () {
    // Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});

Route::group(['as'=>'admin.','prefix' => 'admin','middleware'=>['auth','admin']], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('applications', [DashboardController::class, 'viewApplications'])->name('applications.index');
    Route::get('applications/{id}', [DashboardController::class, 'showApplications'])->name('applications.show');
    Route::patch('applications/accept/{application}', [DashboardController::class, 'acceptApplications'])->name('applications.accept');
    Route::patch('applications/decline/{application}', [DashboardController::class, 'declineApplications'])->name('applications.decline');
});

Route::group(['as'=>'user.','prefix' => 'user','middleware'=>['auth','user']], function () {
    Route::get('dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::resource('applications', ApplicationController::class);
    Route::post('get-classification', [ApplicationController::class, 'getClassification'])->name('get-classification.data');
    Route::get('print_pdf/{application}', [ApplicationController::class, 'printPdf'])->name('print_pdf');
    Route::get('profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('profile', [ProfileController::class, 'update'])->name('profile.update');
});