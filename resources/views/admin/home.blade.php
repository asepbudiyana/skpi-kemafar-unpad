<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <!-- Font Awesome -->
        <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
      rel="stylesheet"
    />
    <!-- Google Fonts -->
    <link
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      rel="stylesheet"
    />
    <!-- MDB -->
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"
      rel="stylesheet"
    />
</head>
<body>

<header>
  <!-- Intro settings -->
  <style>
    /* Default height for small devices */
    #intro-example {
      height: 400px;
    }

    /* Height for devices larger than 992px */
    @media (min-width: 992px) {
      #intro-example {
        height: 600px;
      }
    }
  </style>

  <!-- Navbar -->
  <!-- <nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container-fluid">
      <button
        class="navbar-toggler"
        type="button"
        data-mdb-toggle="collapse"
        data-mdb-target="#navbarExample01"
        aria-controls="navbarExample01"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarExample01">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item active">
            <a class="nav-link" aria-current="page" href="{{ route('user.dashboard') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('user.applications.index') }}">Pengajuan SKPI</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('user.profile.edit') }}">Profil</a>
          </li>
        </ul>
      </div>
    </div>
  </nav> -->
  <!-- Navbar -->

  <!-- Background image -->
  <div
    id="intro-example"
    class="p-5 text-center bg-image"
    style="background-image: url('http://kema.farmasi.unpad.ac.id/wp-content/uploads/2018/07/kemvlog.png');"
  >
    <div class="mask" style="background-color: rgba(0, 0, 0, 0.7);">
      <div class="d-flex justify-content-center align-items-center h-100">
        <div class="text-white">
          <h2 class="mb-3">Selamat datang di halaman dashboard</h2>
          <h1 class="mb-4">{{ Auth::user()->name }}</h1>
          <a
            class="btn btn-outline-light btn-lg m-2"
            href="{{ route('admin.dashboard') }}"
            role="button"
            rel="nofollow"
            >Home</a
          >
          <a
            class="btn btn-outline-light btn-lg m-2"
            href="{{ route('admin.applications.index') }}"
            role="button"
            rel="nofollow"
            >Pengajuan SKPI</a
          >
          <a
            class="btn btn-outline-light btn-lg m-2"
            href="{{ route('logout') }}"
            role="button"
            >Logout</a
          >
        </div>
      </div>
    </div>
  </div>
  <!-- Background image -->
</header>

<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"
></script>
</body>
</html>