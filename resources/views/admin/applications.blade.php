@extends('admin.layout')
 
@section('content')
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Daftar Pengajuan SKPI</a>
    <div class="d-flex align-items-center">
        <a href="{{  route('admin.dashboard') }}" class="btn btn-primary me-3">
            Back
        </a>
    </div>
  </div>
</nav>

<div class="card">
  <div class="card-body">   
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif
  
  <ul class="nav nav-tabs" role="tablist" data-tabs="tabs">
    <li class="nav-item">
      <a class="nav-link active" href="#pending" role="tab" data-toggle="tab">Pending</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#accepted" role="tab" data-toggle="tab">Accepted</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#declined" role="tab" data-toggle="tab">Declined</a>
    </li>
  </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="pending">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>User</th>
                <th>BEM</th>
                <th>Dekanat</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($applications_pending as $application)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $application->name }}</td>
                <td>
                @if ($application->bem_confirmed == 1)
                    <i class="fas fa-check"></i>
                @elseif ($application->bem_confirmed == 0)
                    <i class="fas fa-clock"></i>
                @else
                    <i class="fas fa-times"></i>
                @endif
                </td>
                <td>
                @if ($application->dekanat_confirmed == 1)
                    <i class="fas fa-check"></i>
                @elseif ($application->dekanat_confirmed == 0)
                    <i class="fas fa-clock"></i>
                @else
                    <i class="fas fa-times"></i>
                @endif
                </td>
                <td>
                    <!-- <form action="{{ route('user.applications.destroy',$application->id) }}" method="POST"> -->
                        <a class="btn btn-primary" href="{{ route('admin.applications.show',$application->id) }}">Show</a>

                        <form method="POST" action="{{ route('admin.applications.accept',$application->id) }}">
                          @csrf
                          @method('patch')
                          <button type="submit" class="btn btn-success">Accept</button>
                        </form>

                        <form method="POST" action="{{ route('admin.applications.decline',$application->id) }}">
                          @csrf
                          @method('patch')
                          <button type="submit" class="btn btn-danger">Decline</button>
                        </form>

                        @csrf
                        <!-- @method('DELETE') -->

                        <!-- <button type="submit" class="btn btn-danger">Delete</button> -->
                    <!-- </form> -->
                </td>
            </tr>
            @endforeach
        </table>

        {!! $applications_pending->fragment('pending')->links("pagination::bootstrap-4") !!}
      </div>
      <div role="tabpanel" class="tab-pane" id="accepted">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>User</th>
                <th>BEM</th>
                <th>Dekanat</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($applications_accepted as $application)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $application->name }}</td>
                <td>
                @if ($application->bem_confirmed == 1)
                    <i class="fas fa-check"></i>
                @elseif ($application->bem_confirmed == 0)
                    <i class="fas fa-clock"></i>
                @else
                    <i class="fas fa-times"></i>
                @endif
                </td>
                <td>
                @if ($application->dekanat_confirmed == 1)
                    <i class="fas fa-check"></i>
                @elseif ($application->dekanat_confirmed == 0)
                    <i class="fas fa-clock"></i>
                @else
                    <i class="fas fa-times"></i>
                @endif
                </td>
                <td>
                    <!-- <form action="{{ route('user.applications.destroy',$application->id) }}" method="POST"> -->
                        <a class="btn btn-primary" href="{{ route('admin.applications.show',$application->id) }}">Show</a>

                        <form method="POST" action="{{ route('admin.applications.accept',$application->id) }}">
                          @csrf
                          @method('patch')
                          <button type="submit" class="btn btn-success">Accept</button>
                        </form>

                        <form method="POST" action="{{ route('admin.applications.decline',$application->id) }}">
                          @csrf
                          @method('patch')
                          <button type="submit" class="btn btn-danger">Decline</button>
                        </form>

                        @csrf
                        <!-- @method('DELETE') -->

                        <!-- <button type="submit" class="btn btn-danger">Delete</button> -->
                    <!-- </form> -->
                </td>
            </tr>
            @endforeach
        </table>

        {!! $applications_accepted->fragment('accepted')->links("pagination::bootstrap-4") !!}
      </div>
      <div role="tabpanel" class="tab-pane" id="declined">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>User</th>
                <th>BEM</th>
                <th>Dekanat</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($applications_declined as $application)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $application->name }}</td>
                <td>
                @if ($application->bem_confirmed == 1)
                    <i class="fas fa-check"></i>
                @elseif ($application->bem_confirmed == 0)
                    <i class="fas fa-clock"></i>
                @else
                    <i class="fas fa-times"></i>
                @endif
                </td>
                <td>
                @if ($application->dekanat_confirmed == 1)
                    <i class="fas fa-check"></i>
                @elseif ($application->dekanat_confirmed == 0)
                    <i class="fas fa-clock"></i>
                @else
                    <i class="fas fa-times"></i>
                @endif
                </td>
                <td>
                    <!-- <form action="{{ route('user.applications.destroy',$application->id) }}" method="POST"> -->
                        <a class="btn btn-primary" href="{{ route('admin.applications.show',$application->id) }}">Show</a>

                        <form method="POST" action="{{ route('admin.applications.accept',$application->id) }}">
                          @csrf
                          @method('patch')
                          <button type="submit" class="btn btn-success">Accept</button>
                        </form>

                        <form method="POST" action="{{ route('admin.applications.decline',$application->id) }}">
                          @csrf
                          @method('patch')
                          <button type="submit" class="btn btn-danger">Decline</button>
                        </form>

                        @csrf
                        <!-- @method('DELETE') -->

                        <!-- <button type="submit" class="btn btn-danger">Delete</button> -->
                    <!-- </form> -->
                </td>
            </tr>
            @endforeach
        </table>

        {!! $applications_declined->fragment('declined')->links("pagination::bootstrap-4") !!}
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  if(window.location.hash) {
    document.querySelector('.active').classList.remove('active');
    document.querySelector('[href="'+window.location.hash+'"]').classList.add('active');
    document.querySelector('.tab-pane').classList.remove('active');
    document.querySelector('[id="'+window.location.hash.replace('#', '')+'"]').classList.add('active');
  }
</script>
@endsection