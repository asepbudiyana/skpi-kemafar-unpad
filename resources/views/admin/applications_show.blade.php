@extends('admin.layout')
  
@section('content')
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Detail Pengajuan SKPI</a>
    <div class="d-flex align-items-center">
        <a href="{{ route('admin.applications.index') }}" class="btn btn-primary me-3">
            Back
        </a>
    </div>
  </div>
</nav>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                {{ $application->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NPM:</strong>
                {{ $application->npm }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Konfirmasi BEM:</strong>
                @if ($application->bem_confirmed == 1)
                    Disetujui
                @elseif ($application->bem_confirmed == 0)
                    Pending
                @else
                    Ditolak
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Konfirmasi Dekanat:</strong>
                @if ($application->dekanat_confirmed == 1)
                    Disetujui
                @elseif ($application->dekanat_confirmed == 0)
                    Pending
                @else
                    Ditolak
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Data Dukung:</strong>
            </div>
        </div>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="pending">
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Aktivitas</th>
                    <th>Klasifikasi</th>
                    <th>File</th>
                </tr>
                @foreach ($support_datas as $item)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $item->activity }}</td>
                    <td>{{ $item->classification }}</td>
                    <td><a href="{{ $item->full_file }}" target="_blank">{{ $item->file }}</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection