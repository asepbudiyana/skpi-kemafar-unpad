<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Font Awesome -->
        <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
      rel="stylesheet"
    />
    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
    
    <!-- MDB -->
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"
      rel="stylesheet"
    />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet">
    />
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</head>
<body class="sb-nav-fixed" style="background-color: #eee;
    background-image:url({{asset('/images/bgdashboard.png')}});
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: bottom right; 
    background-size: contain;">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-blues">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="index.html">SKPI KEMAFAR</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            </div>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('user.profile.edit') }}">Edit Profil</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark bg-blues"  style="background-color: #0a446b; " id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Beranda</div>
                            <a class="nav-link" href="{{ route('user.dashboard') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                                Beranda
                            </a>
                            <div class="sb-sidenav-menu-heading">Pengajuan</div>
                            <a class="nav-link" href="{{ route('user.applications.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Daftar Pengajuan
                            </a>
                            <a class="nav-link" href="{{ route('user.applications.create') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-plus-circle"></i></div>
                                Pengajuan Baru
                                </a>
                            <div class="sb-sidenav-menu-heading">Profil</div>
                            <a class="nav-link" href="{{ route('user.profile.edit') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-user-circle"></i></div>
                                Edit Profil
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer" style="background-color: #0f5381;">
                        <div class="small">Login sebagai:</div>
                        {{ Auth::user()->name }}
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h2 class="mt-4 mb-4 text-muted"><i class="fas fa-home mr-2"></i>Beranda</h2>
                        <div class="card mb-4">
                            <h5 class="card-body">
                                Selamat Datang di Web Pengajuan Surat Keterangan Pendamping Ijazah <br>
                                Keluarga Mahasiswa Fakultas Farmasi <br>
                                Universitas Padjajaran
                            </h4>
                        </div>
                    </div>
                </main>
                <!-- <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2021</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer> -->
            </div>
        </div>
        <script src="{{asset('js/scripts.js')}}"></script>
        <!-- <script src="js/datatables-simple-demo.js"></script> -->
    </body>
</html>