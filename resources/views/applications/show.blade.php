@extends('applications.layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Detail Pengajuan SKPI</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('user.applications.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                {{ $application->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Konfirmasi BEM:</strong>
                {{ $application->bem_confirmed }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Konfirmasi Dekanat:</strong>
                {{ $application->dekanat_confirmed }}
            </div>
        </div>
    </div>
@endsection