<!DOCTYPE html>
<html>
<head>
	<title>Surat Keterangan Pendamping Ijazah</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Surat Keterangan Pendamping Ijazah</h4>
		<h6><a target="_blank" href="#">Kemafar Unpad</a></h5>
	</center>
	
	<strong>Nama:</strong>
  <p>{{ $application->name }}</p>

	<strong>NPM:</strong>
  <p>{{ $application->npm }}</p>

	<strong>Konfirmasi BEM:</strong>
	@if ($application->bem_confirmed == 1)
      <p>Disetujui</p>
  @elseif ($application->bem_confirmed == 0)
      <p>Pending</p>
  @else
      <p>Ditolak</p>
  @endif

	<strong>Konfirmasi Dekanat:</strong>
	@if ($application->dekanat_confirmed == 1)
      <p>Disetujui</p>
  @elseif ($application->dekanat_confirmed == 0)
      <p>Pending</p>
  @else
      <p>Ditolak</p>
  @endif

	<table class='table table-bordered'>
		<thead>
			<tr>
        <th>No</th>
        <th>Aktivitas</th>
        <th>Klasifikasi</th>
			</tr>
		</thead>
		<tbody>
		@php $i=1 @endphp
		@foreach ($support_datas as $item)
			<tr>
				<td>{{$i++}}</td>
				<td>{{$item->activity }}</td>
        <td>{{ $item->classification }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	

</body>
</html>