@extends('applications.layout')
  
@section('content')
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Buat Pengajuan SKPI Baru</a>
  </div>
</nav>
   
<div class="card">
  <div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('user.applications.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

      <!-- 2 column grid layout with text inputs for the first and last names -->
      <div class="row mb-4">
        <div class="col">
          <div class="form-outline">
            <input type="text" name="name" value="{{ $user->name }}" id="form6Example1" class="form-control" readonly/>
            <label class="form-label" for="form6Example1">Nama</label>
          </div>
        </div>
        <div class="col">
          <div class="form-outline">
            <input type="text" name="npm" value="{{ $user->npm }}" id="form6Example2" class="form-control" readonly/>
            <label class="form-label" for="form6Example2">NPM</label>
          </div>
        </div>
      </div>

        <table class="table table-bordered" id="dynamicAddRemove">
            <tr>
                <th>Nama Kegiatan</th>
                <th>Klasifikasi</th>
                <th>File Data Dukung</th>
                <th>Aksi</th>
            </tr>
            <tr>
                <td>
                    <!-- <input type="text" name="addMoreInputFields[0][activity]" placeholder="Masukan Nama Kegiatan" class="form-control" /> -->
                    <select name="addMoreInputFields[0][activity]" id="activity_0" onchange="showClassification(0)" class="form-select">
                        <option value="" disabled selected>== Pilih Nama Kegiatan ==</option>
                        @foreach ($activities as $item)
                            <option value="{{ $item['id'] }}">{{ $item['activity'] }}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <!-- <input type="text" name="addMoreInputFields[0][classification]" placeholder="Masukan Klasifikasi" class="form-control" /> -->
                    <select name="addMoreInputFields[0][classification]" id="classification_0" class="form-select">
                        <option value="" disabled selected>== Pilih Klasifikasi ==</option>
                    </select>
                </td>
                <td><input type="file" name="addMoreInputFields[0][file]" placeholder="Masukan File Data Dukung" class="form-control" />
                </td>
                <td><button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary">Tambah</button></td>
            </tr>
        </table>

      <!-- Submit button -->
      <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
    </form>
  </div>
</div>

<!-- JavaScript -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    var i = 0;
    $("#dynamic-ar").click(function () {
        ++i;
        $("#dynamicAddRemove").append(
            `<tr>
            <td>
            <select name="addMoreInputFields[${i}][activity]" id="activity_${i}" onchange="showClassification(${i})" class="form-control">
                <option value="">== Pilih Nama Kegiatan ==</option>
                @foreach ($activities as $item)
                    <option value="{{ $item['id'] }}">{{ $item['activity'] }}</option>
                @endforeach
            </select>
            </td>
            <td>
            <select name="addMoreInputFields[${i}][classification]" id="classification_${i}" class="form-control">
                <option value="">== Pilih Klasifikasi ==</option>
            </select>    
            </td>
            <td><input type="file" name="addMoreInputFields[${i}][file]" placeholder="Masukan File Data Dukung" class="form-control" /></td>
            <td><button type="button" class="btn btn-outline-danger remove-input-field">Hapus</button></td>
            </tr>`
            );
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    $(function () {
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#activitys').on('change', function () {
            $.ajax({
                url: '{{ route('user.get-classification.data') }}',
                method: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: $(this).val()
                },
                success: function (response) {
                    $('#classification').empty();

                    $.each(response, function (index) {
                        $('#classification').append(new Option(response[index].classification, response[index].id))
                    })
                }
            })
            // axios.post('{{ route('user.get-classification.data') }}', {id: $(this).val()})
            // .then(function (response) {
            //     $('#classification').empty();

            //     $.each(response.data, function (id, name) {
            //         $('#classification').append(new Option(name, id))
            //     })
            // });
            
        });
    });
</script>
<script>
function showClassification(code) {
    $.ajax({
        url: '{{ route('user.get-classification.data') }}',
        method: 'POST',
        data: {
            _token: "{{ csrf_token() }}",
            id: $(`#activity_${code}`).val()
        },
        success: function (response) {
            $(`#classification_${code}`).empty();
            $.each(response, function (index) {
                $(`#classification_${code}`).append(new Option(response[index].classification, response[index].id))
            })
        }
    })
}
</script>
@endsection