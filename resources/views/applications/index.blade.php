@extends('applications.layout')
 
@section('content')
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Pengajuan SKPI Anda</a>
  </div>
</nav>

<div class="card">
    <div class="card-body">   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>User</th>
            <th>BEM</th>
            <th>Dekanat</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($applications as $application)
        <tr>
            <th>{{ ++$i }}</th>
            <td>{{ $application->name }}</td>
            <td>
            @if ($application->bem_confirmed == 1)
                <i class="fas fa-check"></i>
            @elseif ($application->bem_confirmed == 0)
                <i class="fas fa-clock"></i>
            @else
                <i class="fas fa-times"></i>
            @endif
            </td>
            <td>
            @if ($application->dekanat_confirmed == 1)
                <i class="fas fa-check"></i>
            @elseif ($application->dekanat_confirmed == 0)
                <i class="fas fa-clock"></i>
            @else
                <i class="fas fa-times"></i>
            @endif
            </td>
            <td>
                <form action="{{ route('user.applications.destroy',$application->id) }}" method="POST">

                    @if ($application->bem_confirmed == 1 && $application->dekanat_confirmed == 1)
                        <a class="btn btn-info" href="{{ route('user.print_pdf', $application) }}" target="_blank">Print</a>
                    @else
                        <button class="btn btn-light" disabled>Print</button>
                    @endif
                    <!-- <a class="btn btn-info" href="{{ route('user.applications.show',$application->id) }}">Show</a> -->
    
                    <!-- <a class="btn btn-primary" href="{{ route('user.applications.edit',$application->id) }}">Edit</a> -->
   
                    @csrf
                    <!-- @method('DELETE') -->
      
                    <!-- <button type="submit" class="btn btn-danger">Delete</button> -->
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $applications->links("pagination::bootstrap-4") !!}

    </div>
</div>

@endsection