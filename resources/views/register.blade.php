<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
      rel="stylesheet"
    />
    <!-- Google Fonts -->
    <link
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      rel="stylesheet"
    />
    <!-- MDB -->
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"
      rel="stylesheet"
    />
</head>
<body style="background-color: #eee;
    background-image:url({{asset('/images/bgdashboard.png')}});
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: bottom right; 
    background-size: contain;">
<section class="h-100">
  <div class="container py-4 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col">
        <div class="card card-registration my-4">
          <div class="row g-0">
            <div class="col-xl-6 d-none d-xl-block">
              <img
                src = "{{ asset('/images/illusreg.png') }}"
                alt="Sample photo"
                class="img-fluid"
                style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem;"
              />
            </div>
            <div class="col-xl-6">
              <div class="card-body p-md-5 text-black">
                <h3 class="mb-3 text-uppercase fw-bold text-center">Form Register</h3>
                
                <form action="{{ route('register') }}" method="post">
                @csrf
                @if(session('errors'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        Something it's wrong:
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-outline mb-4">
                  <input type="text" name="name" id="form3Example9" class="form-control form-control-lg" />
                  <label class="form-label" for="form3Example9">Nama Lengkap</label>
                </div>

                <div class="form-outline mb-4">
                  <input type="text" name="email" id="form3Example97" class="form-control form-control-lg" />
                  <label class="form-label" for="form3Example97">Email</label>
                </div>

                <div class="row">
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="password" name="password" id="form3Example1m" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1m">Password</label>
                    </div>
                  </div>
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="password" name="password_confirmation" id="form3Example1n" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1n">Konfirmasi Password</label>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="number" name="npm" id="form3Example1m1" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1m1">NPM</label>
                    </div>
                  </div>
                  <div class="col-md-6 mb-4">
                    <div class="form-outline">
                      <input type="tel" name="phone" id="form3Example1n1" class="form-control form-control-lg" />
                      <label class="form-label" for="form3Example1n1">Nomor Telepon</label>
                    </div>
                  </div>
                </div>

                <div class="form-outline mb-4">
                  <input type="text" name="address" id="form3Example8" class="form-control form-control-lg" />
                  <label class="form-label" for="form3Example8">Alamat</label>
                </div>

                <div class="d-flex justify-content-end py-3 justify-content-center">
                  <!-- <button type="reset" class="btn btn-light btn-lg">Reset</button> -->
                  <button type="submit" class="btn btn-primary btn-lg ms-2">Register</button>
                </div>

                <div class="form-outline mt-3">
                    <p class="text-center">Sudah punya akun? <a href="{{ route('login') }}">Login</a> sekarang!</p>
                </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"
></script>
</body>
</html>