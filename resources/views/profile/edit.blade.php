@extends('profile.layout')

@section('content')
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Halaman Update Profile</a>
  </div>
</nav>

<div class="card">
    <div class="card-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif  

    <form method="POST" action="{{ route('user.profile.update') }}">
      @method('patch')
      @csrf

      <!-- Name input -->
      <div class="form-outline mb-4">
        <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" autocomplete="name" autofocus/>
        <label class="form-label" for="form3Example3">Nama Lengkap</label>
        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <!-- Email input -->
      <div class="form-outline mb-4">
        <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" autocomplete="email" />
        <label class="form-label" for="form3Example3">Email</label>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <!-- 2 column grid layout with text inputs for the first and last names -->
      <div class="row mb-4">
        <div class="col">
          <div class="form-outline">
            <input type="number" id="npm" class="form-control @error('npm') is-invalid @enderror" name="npm" value="{{ old('npm', $user->npm) }}" autocomplete="npm" autofocus />
            <label class="form-label" for="form3Example1">NPM</label>
            @error('npm')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div>
        <div class="col">
          <div class="form-outline">
            <input type="phone" id="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone', $user->phone) }}" autocomplete="phone"/>
            <label class="form-label" for="form3Example2">Nomor Telepon</label>
            @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
        </div>
      </div>
        
      <!-- Password input -->
      <div class="form-outline mb-4">
        <input type="text" id="addess" class="form-control" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address', $user->address) }}" autocomplete="address"/>
        <label class="form-label" for="form3Example4">Alamat</label>
        @error('address')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
    
      <!-- Submit button -->
      <button type="submit" class="btn btn-primary btn-block mb-4">Update Profile</button>
    
    </form>
    </div>
</div>

@endsection